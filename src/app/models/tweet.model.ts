export interface Tweet {
    tweetURL: string;
    favorites: string;
    retweets: string;
    tweetText: string;
    createdat : string;
    username : string;
}