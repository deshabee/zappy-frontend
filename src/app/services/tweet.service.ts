import { Injectable }   from '@angular/core';
import { HttpClient }   from '@angular/common/http';
import { Observable }   from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Tweet } from '../models/tweet.model';
@Injectable()
export class TweetService {
  private serviceUrl = 'http://localhost:8081/api/get_all_tweets';
  
  constructor(private http: HttpClient) { }
  
  getTweet(): Observable<Tweet[]> {
  	console.log(this.http.get<Tweet[]>(this.serviceUrl))
    return this.http.get<Tweet[]>(this.serviceUrl);
  }
  
}