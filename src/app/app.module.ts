import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { TweetstableComponent } from './components/tweetstable/tweetstable.component';
import { MatTableModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { TweetService } from './services/tweet.service';

@NgModule({
  declarations: [
    AppComponent,
    TweetstableComponent
  ],
  imports: [
    BrowserModule,
    
    HttpClientModule,
    MatTableModule
  ],
  providers: [TweetService],
  bootstrap: [AppComponent]
})
export class AppModule { }
