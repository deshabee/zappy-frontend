import { Component, OnInit } from '@angular/core';
import { TweetService } from '../../services/tweet.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {DataSource} from '@angular/cdk/collections';
import { Tweet } from '../../models/tweet.model';
@Component({
  selector: 'tweetstable',
  templateUrl: './tweetstable.component.html',
  styleUrls: ['./tweetstable.component.css']
})
export class TweetstableComponent implements OnInit {
  dataSource = new UserDataSource(this.tweetService);
  displayedColumns = ['tweetText', 'retweets', 'favorites', 'tweetURL' , 'createdat' , 'username'];
  constructor(private tweetService: TweetService) { }
  
  ngOnInit() {
  }
}
export class UserDataSource extends DataSource<any> {
  constructor(private tweetService: TweetService) {
    super();
  }
  connect(): Observable<Tweet[]> {
    return this.tweetService.getTweet();
  }
  disconnect() {}
}