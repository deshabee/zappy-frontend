# Zappyfrontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

###The machine should have nodejs , npm and angular/cli installed.
###The APP requires the API to be already running on the port 8081.

### Install The used packages
```sh 
$ npm install
```

### How to run the system on your localmachine
```sh 
$ ng serve --open
```